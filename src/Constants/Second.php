<?php
declare(strict_types=1);

namespace Madoka\Constants;

/**
 * 时间秒数常量
 * Class Second
 * @package Madoka\Constants
 */
class Second
{
    /**
     * 一分钟
     */
    const MINUTE = 60;
    /**
     * 一小时
     */
    const HOUR = 3600;
    /**
     * 一天
     */
    const DAY = 86400;
    /**
     * 一周
     */
    const WEEK = 604800;
    /**
     * 一月(30天)
     */
    const MONTH = 2592000;
    /**
     * 一季度(90天)
     */
    const QUARTER = 7776000;
    /**
     * 一年
     */
    const YEAR = 31536000;
    /**
     * 十年
     */
    const DECADE = 315360000;
}