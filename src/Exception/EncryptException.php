<?php

declare(strict_types=1);

namespace Madoka\Exception;

use RuntimeException;

/**
 * Encrypt Exception
 * Class EncryptException
 * @package Madoka\Exception
 */
class EncryptException extends RuntimeException
{

}