<?php

declare(strict_types=1);

namespace Madoka\Exception;

use RuntimeException;

/**
 * RPC Service Exception
 * Class RpcException
 * @package Madoka\Exception
 */
class RpcException extends RuntimeException
{

}