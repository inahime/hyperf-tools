<?php

declare(strict_types=1);

namespace Madoka\Exception;

use RuntimeException;

/**
 * Class TemplateException
 * @package Madoka\Exception
 */
class TemplateException extends RuntimeException
{

}