<?php

declare(strict_types=1);

namespace Madoka\Security;

interface IntegerPackInterface
{
    /**
     * @return string
     */
    public function getPack(): string;
}